import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from tissueviewer.mesh import tvMeshImageVTK
import fast_fit as ff
from handy_functions import *
import vtk
from vtk.util.numpy_support import vtk_to_numpy, numpy_to_vtk
from openalea.container.topomesh_algo import border
from numpy import dtype
from scipy.ndimage import binary_dilation, binary_erosion, binary_closing
from scipy.ndimage.morphology import binary_opening
from reportlab.lib.randomtext import objects

# """
# load image of SAM
# """
# fitval = 122
# fit,_ = readImages('/home/maxbrambach/workspace/Project Snakebite/reference_images/nt1_gb6.tif')
# fit[fit==0] = fitval # set 'inside SAM' to fival - is 0 in file
#    
# """
# create mesh from image
# """
# SAM = tvMeshImageVTK(fit, removeBoundaryCells = False, reduction = 0, smooth_steps = 0)
# SAM = SAM[fitval]



"""
read polydata
"""
reader = vtk.vtkXMLPolyDataReader()
reader.SetFileName('reference_images/SAM500_cs.vtp')
reader.Update()
SAM = reader.GetOutput()


# """
# clean and smooth
# """
# cleanPolyData = vtk.vtkCleanPolyData()
# cleanPolyData.SetInput(SAM)
# # cleanPolyData.SetInputConnection(SAM.GetOutputPort())
#   
# smoothFilter = vtk.vtkSmoothPolyDataFilter()
# smoothFilter.SetInputConnection(cleanPolyData.GetOutputPort())
# smoothFilter.SetNumberOfIterations(100)
# smoothFilter.SetRelaxationFactor(1.)
# # smoothFilter.FeatureEdgeSmoothingOff()
# # smoothFilter.BoundarySmoothingOn()
# smoothFilter.Update()
#  
# # print smoothFilter.GetOutput().GetPoints().GetData()




"""
color setup
"""
# Colour transfer function.
colorsteps = 10
ctf = vtk.vtkColorTransferFunction()
ctf.SetColorSpaceToDiverging()
ctf.AddRGBPoint(0.0, 0.230, 0.299, 0.754)
ctf.AddRGBPoint(1.0, 0.706, 0.016, 0.150)
cc = list()
for i in range(colorsteps):
    cc.append(ctf.GetColor(float(i) / float(colorsteps-1.))) 
    
# Lookup table.
lut = vtk.vtkLookupTable()
lut.SetNumberOfColors(colorsteps)
for i, item in enumerate(cc):
    lut.SetTableValue(i, item[0], item[1], item[2], 1.0)
    lut.SetRange(-.01, .01)
lut.Build()

"""
curvature
"""
curvature = vtk.vtkCurvatures()
# curvature.SetCurvatureTypeToMean()
curvature.SetCurvatureTypeToMean()
curvature.SetInput(SAM)
curvature.Update()
# curvature.SetInputConnection(smoothFilter.GetOutputPort())


"""
Threshold Filter
"""
borders = vtk.vtkThreshold()
# borders.ThresholdByUpper(0.)
borders.ThresholdByLower(0.)
borders.SetInputConnection(curvature.GetOutputPort())


geofilter = vtk.vtkGeometryFilter()
geofilter.SetInputConnection(borders.GetOutputPort())
geofilter.Update()


def array_from_vtk_polydata(poly,size=[]):
    if np.shape(size) == np.shape([]):
        size = np.array(poly.GetPoints().GetBounds(),dtype='int')[1::2]
    indices = np.array(vtk_to_numpy(poly.GetPoints().GetData()),dtype='int')
    out = np.zeros(size)
    out[indices[:,0]-1,indices[:,1]-1,indices[:,2]-1] = 1
    return np.array(out)
 
 

# def vtk_polydata_from_array(array):
# #     out = vtk.vtkPolyData()
# #     longarray = (numpy_to_vtk(np.nonzero(array)))
# #     out = vtk.vtkPointData()
# #     out.SetInputData(longarray)
# #     NumPy_data_shape = array.shape
# #     VTK_data = numpy_to_vtk(num_array=array.ravel(), deep=True, array_type=vtk.VTK_POINT_DATA)
# 
#     dataImporter = vtk.vtkImageImport()
#     data_string = array.tostring()
#     dataImporter.CopyImportVoidPointer(data_string, len(data_string))
#     # The type of the newly imported data is set to unsigned char (uint8)
#     dataImporter.SetDataScalarTypeToInt()
#     # Because the data that is imported only contains an intensity value (it isnt RGB-coded or someting similar), the importer
#     # must be told this is the case.
#     dataImporter.SetNumberOfScalarComponents(1)
#     # The following two functions describe how the data is stored and the dimensions of the array it is stored in. For this
#     # simple case, all axes are of length 75 and begins with the first element. For other data, this is probably not the case.
#     # I have to admit however, that I honestly dont know the difference between SetDataExtent() and SetWholeExtent() although
#     # VTK complains if not both are used.
#     x,y,z = np.shape(array)
#     dataImporter.SetDataExtent(0, x, 0, y, 0, z)
#     dataImporter.SetWholeExtent(0, x, 0, y, 0, z)
#     dataImporter.Update()
#     out = vtk.vtkImageDataGeometryFilter()
#     out.SetInputConnection(dataImporter.GetOutputPort())
#     out.Update()
#     return out.GetOutput()#out
#     
# 
# 
# 
# slice_mesh = ff.pointcloud(model=array_from_vtk_polydata(geofilter.GetOutput()) != 0)
# mesh = ff.pointcloud(model=array_from_vtk_polydata(reader.GetOutput()) != 0)
# 
# 
# 
# sliced = ff.pointcloud(model=np.logical_and(mesh.model, np.logical_not(slice_mesh.model)))
# clip = vtk.vtkClipPolyData()
# 
# 
# print geofilter.GetOutput().GetPoints().GetData().GetArray()
# 
# 
# 
# 
# exit()



"""
Connectivity Filter
"""
connect = vtk.vtkPolyDataConnectivityFilter()
connect.SetExtractionModeToPointSeededRegions()
connect.SetInputConnection(geofilter.GetOutputPort())
connect.Update()
points = connect.GetInput().GetNumberOfPoints()

res = 50
thresh = int(points/float(res))
objects_vtk = []
lastobj = []
# objects_np = []
for i in range(2,res):
    connect.InitializeSeedList()
    connect.AddSeed(int(points/res*float(i))-1)
    connect.Update()
    temp = []
    temp = connect.GetOutput()
    temp.Update()
    if temp.GetNumberOfPoints() > thresh:
        if not(temp.GetNumberOfPoints() in lastobj):
            objects_vtk.append(vtk.vtkPolyData())
            objects_vtk[-1].DeepCopy(temp)
            lastobj.append(temp.GetNumberOfPoints())
#             objects_np.append(array_from_vtk_polydata(temp,size=np.array(geofilter.GetOutput().GetPoints().GetBounds(),dtype='int')[1::2]))


objects_vtk = np.flipud(sort_a_along_b(objects_vtk, lastobj))


# print objects_vtk
#  
# exit()




# shape_obj = np.shape(array_from_vtk_polydata(objects_vtk[0]))
# objects = np.zeros((shape_obj[0],shape_obj[1],shape_obj[2],np.shape(objects_vtk)[0]))
# for i in range(np.shape(objects_vtk)[0]):
#     objects[:,:,:,i] = objects_np[i]
# 
# print lastobj
# print np.shape(objects_np)
# # print objects_np[:,:,:,0]
# view3d(objects_np.sum(3), contour=True)
            

# objects_np = np.zeros()
                  

# # print points
# res = 30
# for i in range(res):
#     i = i+1
#     connect.AddSeed(int(points/res*float(i))-1)
# connect.DeleteSeed(int(points/res*float(1))-1)
#     print int(points/10.*float(i)-1)
# exit()

# print np.shape(objects)



"""
Mapper,actor
"""
Mappers = []
Actors = []
# Renderers = []
# renderwindow = vtk.vtkRenderWindow()

colors = [(1.,1.,1.),(1.,0.,0.),(0.,1.,0.),(0.,0.,1.),(1.,1.,0.),(1.,0.,1.),(0.,1.,1.),(1.,0.5,0.5),(0.5,1.,0.5),(0.5,0.5,1.),(1.,1.,0.5),(1.,0.5,1.),(0.5,1.,1.)]

# print colors[6]

# exit()

for i in range(np.shape(objects_vtk)[0]):
    print i
    mapper = vtk.vtkPolyDataMapper()
    mapper.SetInput(objects_vtk[i])
    mapper.ScalarVisibilityOff()
    mapper.Update()
    Mappers.append(mapper)
    actor = vtk.vtkActor()
    actor.SetMapper(Mappers[i])
    actor.GetProperty().SetColor(colors[i])
    Actors.append(actor)
#     render = vtk.vtkRenderer()
#     render.AddActor(Actors[i])
#     Renderers.append(render)
#     renderwindow.AddRenderer(Renderers[i])
 
# SAMmapper = vtk.vtkPolyDataMapper()
# SAMmapper.SetInputConnection(allobj.GetOutputPort())
# # SAMmapper.SetLookupTable(lut)
# # SAMmapper.SetUseLookupTableScalarRange(1)
#   
# SAMactor = vtk.vtkActor()
# SAMactor.SetMapper(SAMmapper)
# SAMactor.RotateX(-100.0)
# SAMactor.RotateY(-50.0)
# SAMactor.RotateZ(-90.0)
"""
Renderer and Render Window
"""
 
render = vtk.vtkRenderer()
for i in range(np.shape(objects_vtk)[0]):
    render.AddActor(Actors[i])
#     render.AddActor(SAMactor)
renderwindow = vtk.vtkRenderWindow()
renderwindow.AddRenderer(render)
renderwindow.SetSize(600,600)
interactrender = vtk.vtkRenderWindowInteractor()
interactrender.SetRenderWindow(renderwindow)
 
 
interactrender.Initialize()
 
interactrender.Start()



exit()