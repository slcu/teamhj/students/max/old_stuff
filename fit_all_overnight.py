import morphsnakes as ms
from tifffile import imsave # for saving the fit as .tiff
import fast_fit as ff
import numpy as np
from handy_functions import *
from scipy.spatial import distance as dst
from mayavi import mlab as ml
import matplotlib.pyplot as plt




###---------------------script------------------------------###



"""
t1.tif
"""
image,_ = ff.readImages('t1.tif')
A = ff.pointcloud(image)
# imsave('overnight/nt3_red3.tif', np.array(A.data,'uint16'))
levelset = ff.getplanes(A.shape())
levelset = ff.setedges(levelset, 0)
levelset[0,:,:] = 0
A.model = levelset
A.run(200,3,1,3)
imsave('overnight/t1-i_200-l1_4-l2_1-s_3_red0.tif', np.array(A.model,'uint16'))


# """
# nt1.tif
# """
# image,_ = ff.readImages('newtiff/t1.tif')
# A = ff.pointcloud(image)
# #imsave('overnight/nt3_red3.tif', np.array(A.data,'uint16'))
# levelset = ff.getplanes(A.shape())
# levelset = ff.setedges(levelset, 0)
# levelset[0,:,:] = 0
# A.model = levelset
# A.run(200,4,1,3)
# imsave('overnight/nt1-i_200-l1_4-l2_1-s_3_red0.tif', np.array(A.model,'uint16'))

# """
# nt2.tif
# """
# image,_ = ff.readImages('newtiff/t2.tif')
# A = ff.pointcloud(image)
# #imsave('overnight/nt3_red3.tif', np.array(A.data,'uint16'))
# levelset = ff.getplanes(A.shape())
# levelset = ff.setedges(levelset, 0)
# levelset[0,:,:] = 0
# A.model = levelset
# A.run(250,4,1,3)
# imsave('overnight/nt2-i_200-l1_4-l2_1-s_3_red0.tif', np.array(A.model,'uint16'))

# """
# nt3.tif
# """
# image,_ = ff.readImages('newtiff/t3.tif')
# A = ff.pointcloud(image)
# #imsave('overnight/nt3_red3.tif', np.array(A.data,'uint16'))
# print 'go go go'
# levelset = ff.getplanes(A.shape())
# levelset = ff.setedges(levelset, 0)
# levelset[0,:,:] = 0
# A.model = levelset
# A.run(250,4,1,3)
# imsave('overnight/nt3-i_200-l1_4-l2_1-s_3_red0.tif', np.array(A.model,'uint16'))