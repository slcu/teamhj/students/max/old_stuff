'''
Created on 4 Jul 2017

@author: maxbrambach
'''
import numpy as np
import vtk
from handy_functions import *


sphere_source = vtk.vtkSphereSource()
sphere_source.SetRadius(10)
sphere_source.SetPhiResolution(100)
sphere_source.SetThetaResolution(100)
sphere_source.Update()

normal = vtk.vtkPolyDataNormals()
normal.SetInput(sphere_source.GetOutput())
normal.SplittingOff()
normal.Update()

decimate = vtk.vtkDecimatePro()
decimate.SetInput(normal.GetOutput())
decimate.SetTargetReduction(.5)
decimate.Update


arrow = vtk.vtkArrowSource()
arrow.Update()

glyph = vtk.vtkGlyph3D()
glyph.SetInput(decimate.GetOutput())
glyph.SetSourceConnection(arrow.GetOutputPort())
glyph.OrientOn()
glyph.SetScaleFactor(10.)
glyph.SetVectorModeToUseNormal()


glyph.Update()

normalMapper = vtk.vtkPolyDataMapper()
normalMapper.SetInput(glyph.GetOutput())
normalActor = vtk.vtkActor()
normalActor.SetMapper(normalMapper)

renderer = vtk.vtkRenderer()
renderer.AddActor(normalActor)

renderwindow = vtk.vtkRenderWindow()
renderwindow.AddRenderer(renderer)
renderwindow.SetSize(600,600)
interactrender = vtk.vtkRenderWindowInteractor()
interactrender.SetRenderWindow(renderwindow)
 
 
interactrender.Initialize()
 
 

    
axes = vtk.vtkAxesActor()
widget = vtk.vtkOrientationMarkerWidget()
widget.SetOutlineColor( 0.9300, 0.5700, 0.1300 )
widget.SetOrientationMarker( axes )
widget.SetInteractor( interactrender )
widget.SetViewport( 0.0, 0.0, 0.4, 0.4 )
widget.SetEnabled( 1 )
widget.InteractiveOn()

renderer.ResetCamera();
renderwindow.Render();
 
 
 
interactrender.Start()